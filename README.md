OSF Final Project
===================

This will be my result after developing the final project for the **OSF Academy Learning Program**. 

----------


Notes
-------------

To generate the CSS files, use the command: 
```bash
sass style.scss ../css/style.css 
```
To also have the file generate automatically each time style.scss is saved, use the command:
```bash
sass --watch style.scss:../css/style.css 
```
