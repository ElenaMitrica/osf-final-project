$(function($){
  $('#arrow').val("");

//search expand and display suggestions from a json file
  var $iconSearch = $('.icon-search'),
      $input = $('.search-input');
  $iconSearch.click(function(e){
    e.preventDefault();
    $input.toggleClass('search-input-open');
  });

  var search = function(regexp, object, target){
    var $resultContainer = $(target);
    var output = '';
    for(x in object){
      $.each(object[x], function(key, value){  
        if((value.search(regexp) !=-1)){
          output += "<li>";
          output +="<h2>" + value+ "</h2>";
          output +="</li>";
        } 
      }); 
    }
    output +="</div>";
    $resultContainer.html(output);
  }; //end search function

  $.ajax({
    dataType : "json",
    method: "GET",
    url: "scripts/search-options.json",
    success: function(data) {
      $input.on('input', function (){
        var $inputValue = $(this).val();
        var myExp = new RegExp($inputValue, 'i');
        if($inputValue.length > 0){
          $('.brand-results').addClass("show");
          $('.category-results').addClass("show");
          search(myExp, data.brands, '.brand-results');
          search(myExp, data.categories, '.category-results');
        } else {
            $('.search-results ul').children().remove();
            $('.brand-results').removeClass("show")
            $('.category-results').removeClass("show");
          }
      }); 
    },
    error: function (jqXHR, textStatus, errorThrown){
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
    }
  });
//list/ grid products page effect
  var $grid = $('.products-page span.icon-grid');
  var $list = $('.products-page span.icon-th-list');
  var $liItems =  $('ul.items');
  $list.click(function(){
     $liItems.addClass('list');
     $grid.css('box-shadow', 'none');
    $list.css('box-shadow', '5px 5px 5px grey');

   });
   $grid.click(function(){
      $liItems.removeClass('list');
      $list.css('box-shadow', 'none');
      $grid.css('box-shadow', '5px 5px 5px grey');
   });

//login/ register popup 
  var $showLogin = $("#show_login");
  function showpopup(show_el) {
      $(show_el).css({"opacity": "1", "z-index":"101"});

  }
  function hidepopup(hide_el) {
      $(hide_el).css({"opacity": "0",  "z-index":"-1"});
  }
   $showLogin.click(function(e){
      e.preventDefault();
      showpopup("#login-form");
      
   });
  $(".close-form").click(function(){
      hidepopup("#login-form");
      hidepopup("#register-form");
  });
  $("#login-form p span").click(function(){
      showpopup("#register-form");
      hidepopup("#login-form");
  });
  $("#register-form p span").click(function(){
      showpopup("#login-form");
      hidepopup("#register-form");
  });

//image slider
  $('div.toggle').first().addClass('active');
  $('.slider-nav').children('li').each(function(){
    $(this).click(function(){
      var $index = $(this).index();
      var $toggle = $('.toggle-slide li')
                                      .eq($index)
                                      .find('div.toggle')
                                      .addClass('active')
                                      .parent()
                                      .siblings()
                                      .find('div.toggle')
                                      .removeClass('active');
      console.log($toggle);
      var $image = $('.slider li')
                                .eq($index)
                                .siblings().removeClass('show-slide').end()
                                .addClass('show-slide');
                       
      console.log($image);
    });
  });
//save email in json file
  $('.newsletter-form').on('submit', function(e) {
    e.preventDefault();
    $.post('scripts/emails.php', $(this).closest('form').serialize(),function(){
      $(".message-success").text("Thank you for signing up for our newsletter!").show('slow');
      setTimeout(function(){
        $(".message-success").hide('slow');
        $('.newsletter-input').val("");
      }, 2000);
    });
  });

//burger-nav functionality
  var $mainNav = $('#main-nav');
  $('.burger-nav').on('click', function(){
      $('#main-nav').toggleClass('display-block');
  });
//blog news update

  var $blogPost = $(".addon .blog");
  var $initialHidden = $(".initial-hidden"),
    i = 0,
    len = $blogPost.length;
  $initialHidden.hide();
  setInterval(function(){
    $blogPost.eq(i++ % len).fadeOut(function(){
        $blogPost.eq((i+1) % len).fadeIn();
      });
  }, 5000); 
//twitter content updates 

 var $twitterContent = $(".twitter-content");
  var $hidden = $(".hidden"),
    j = 0,
    lengt = $twitterContent.length;
  $hidden.hide();
  setInterval(function(){
    $twitterContent.eq(j++ % lengt).fadeOut(400, function(){
        $twitterContent.eq(j % lengt).fadeIn(1000);
      });
  }, 5000); 

//custom select boxes
  $('.wrapper-dropdown').on('click', function() {
    $(this).find('.dropdown').toggleClass('enable'); 
  });
  $('.dropdown li').on('click', function() { // spatiere
    var $this = $(this);
    $this.parent().siblings('span').replaceWith('<span>' + $this.text() + '</span>');
  });

  var addToAll = false;
  var gallery = true;
  var titlePosition = 'inside';
  $(addToAll ? 'img' : 'img.fancybox').each(function(){
    var $this = $(this);
    var title = $this.attr('title');
    var src = $this.attr('data-big') || $this.attr('src');
    var a = $('<a href="#" class="fancybox"></a>').attr('href', src).attr('title', title);
    $this.wrap(a);
  });
  if (gallery)
    $('a.fancybox').attr('rel', 'fancyboxgallery');
  $('a.fancybox').fancybox({
    titlePosition: titlePosition
  });

// easter egg
$('.icon-shopping_cart, .quantity-icons, .icon-star-outline').click(function(e){
  e.preventDefault();
  $qt = $(this).parent().find('.quantity-icons');
  $qt.html(parseInt($qt.html())+1);
});

});
// $.noConflict();
